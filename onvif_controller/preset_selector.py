import sys
import argparse
from pathlib import Path
from PyQt5.QtWidgets import QApplication

from onvif_controller.gui.configuration import Configuration
from onvif_controller.gui.main_window import MainWindow


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('config', type=Path, help='path to configuration file')
    args = parser.parse_args()
    app = QApplication(sys.argv)
    config = Configuration(args.config)
    ex = MainWindow(config)
    return app.exec()


if __name__ == '__main__':
    main()