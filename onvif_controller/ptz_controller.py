import math
import signal
from time import sleep
from xbox360controller import Xbox360Controller
from onvif import ONVIFCamera, ONVIFService


class PtzController:
    XMAX = 0.1
    XMIN = -0.1
    YMAX = 0.1
    YMIN = -0.1

    def __init__(self, ip: str, port: int, user: str, password: str):
        self.camera = ONVIFCamera(ip, port, user, password)
        # Create media service object
        self.media = self.camera.create_media_service()
        # Create ptz service object
        self.ptz: ONVIFService = self.camera.create_ptz_service()

        # Get target profile
        media_profile = self.media.GetProfiles()[0]
        # print(media_profile)

        # Get PTZ configuration options for getting continuous move range
        request = self.ptz.create_type('GetConfigurationOptions')
        request.ConfigurationToken = media_profile.PTZConfiguration.token
        self.ptz_configuration_options = self.ptz.GetConfigurationOptions(request)
        request = self.ptz.create_type('GetConfiguration')
        request.PTZConfigurationToken = media_profile.PTZConfiguration.token
        self.ptz_configuration = self.ptz.GetConfiguration(request)
        self.is_moving = False

        self.move_request = self.ptz.create_type('ContinuousMove')
        self.move_request.ProfileToken = media_profile.token
        # request.Timeout = 0.1
        if self.move_request.Velocity is None:
            self.move_request.Velocity = media_profile.PTZConfiguration.DefaultPTZSpeed
            self.move_request.Velocity.Zoom.x = 0.0
            # request.Velocity = ptz.GetStatus({'ProfileToken': media_profile.token}).Position

        # Get range of pan and tilt
        # NOTE: X and Y are velocity vector
        self.XMAX = self.ptz_configuration_options.Spaces.ContinuousPanTiltVelocitySpace[0].XRange.Max
        self.XMIN = self.ptz_configuration_options.Spaces.ContinuousPanTiltVelocitySpace[0].XRange.Min
        self.YMAX = self.ptz_configuration_options.Spaces.ContinuousPanTiltVelocitySpace[0].YRange.Max
        self.YMIN = self.ptz_configuration_options.Spaces.ContinuousPanTiltVelocitySpace[0].YRange.Min

        self.xbox_controller = Xbox360Controller(0)
        self.xbox_controller.axis_l.when_moved = self.on_controller_move

        self.move_request.Velocity.PanTilt.x = -0.00001
        self.move_request.Velocity.PanTilt.y = 0
        self.__perform_move__(timeout=0.001)

    def close(self):
        self.xbox_controller.close()
        pass

    def on_controller_move(self, axis):
        if self.is_moving:
            return
        self.move_request.Velocity.PanTilt.x = axis.x
        self.move_request.Velocity.PanTilt.y = axis.y
        timeout = math.sqrt(axis.x * axis.x + axis.y * axis.y) * 0.25
        print('x: {:.2f} y: {:.2f} timeout: {:.2f}'.format(axis.x, axis.y, timeout))
        self.__perform_move__(timeout=timeout)

    def __perform_move__(self, timeout: float):
        self.is_moving = True
        stop = self.ptz.create_type('Stop')
        stop.ProfileToken = self.move_request.ProfileToken
        stop.PanTilt = 1
        stop.Zoom = 1

        # Start continuous move
        self.ptz.ContinuousMove(self.move_request)
        # Wait a certain time
        sleep(timeout)
        self.ptz.Stop(stop)
        self.is_moving = False


if __name__ == '__main__':
    controller = PtzController('192.168.1.137', 80, 'user', 'user')
    try:
        signal.pause()
        pass
    except KeyboardInterrupt:
        pass
    controller.close()
