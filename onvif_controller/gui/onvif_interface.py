from time import sleep
from onvif import ONVIFCamera, ONVIFService


class OnvifInterface:
    def __init__(self, ip: str, port: int, user: str, password: str):
        self.is_moving = True
        self.__ip__ = ip
        self.__port__ = port
        self.__user__ = user
        self.__password__ = password
        self.camera = None
        self.reconnect()

    def reconnect(self):
        try:
            print('Connecting to {}@{}:{}'.format(self.__user__, self.__ip__, self.__port__))
            self.camera = ONVIFCamera(self.__ip__, self.__port__, self.__user__, self.__password__)
            # Create media service object
            self.media = self.camera.create_media_service()
            # Create ptz service object
            self.ptz: ONVIFService = self.camera.create_ptz_service()

            # Get target profile
            self.media_profile = media_profile = self.media.GetProfiles()[0]
            # print(media_profile)

            # Get PTZ configuration options for getting continuous move range
            request = self.ptz.create_type('GetConfigurationOptions')
            request.ConfigurationToken = media_profile.PTZConfiguration.token
            self.ptz_configuration_options = self.ptz.GetConfigurationOptions(request)

            request = self.ptz.create_type('GetPresets')
            request.ProfileToken = media_profile.PTZConfiguration.token
            self.presets = self.ptz.GetPresets(request)

            self.move_request = self.ptz.create_type('ContinuousMove')
            self.move_request.ProfileToken = media_profile.token
            # request.Timeout = 0.1
            if self.move_request.Velocity is None:
                self.move_request.Velocity = media_profile.PTZConfiguration.DefaultPTZSpeed
                self.move_request.Velocity.Zoom.x = 0.0
                # request.Velocity = ptz.GetStatus({'ProfileToken': media_profile.token}).Position
            self.is_moving = False
        except Exception as e:
            print('failed to connect to camera:', e)

    def call_preset(self, num):
        goto_preset = self.ptz.create_type('GotoPreset')
        goto_preset.ProfileToken = self.media_profile.token
        goto_preset.PresetToken = num
        self.ptz.GotoPreset(goto_preset)

    def set_preset(self, num):
        set_preset = self.ptz.create_type('SetPreset')
        set_preset.ProfileToken = self.media_profile.token
        set_preset.PresetToken = num
        self.ptz.SetPreset(set_preset)

    def perform_move(self, x: float, y: float, zoom: float, timeout: float):
        if self.is_moving:
            return
        self.is_moving = True
        # Start continuous move
        self.move_request.Velocity.PanTilt.x = x
        self.move_request.Velocity.PanTilt.y = y
        self.move_request.Velocity.Zoom = zoom
        self.ptz.ContinuousMove(self.move_request)
        # Wait a certain time
        sleep(timeout)
        # Stop continuous move
        stop = self.ptz.create_type('Stop')
        stop.ProfileToken = self.move_request.ProfileToken
        stop.PanTilt = 1
        stop.Zoom = 1
        self.ptz.Stop(stop)
        self.is_moving = False

    def perform_absolute_move(self, x: float, y: float, z: float):
        if self.is_moving:
            return
        self.is_moving = True
        self.ptz.RelativeMove({
            'ProfileToken': self.media_profile.token,
            'Position': {
                'PanTilt': {
                    'x': x,
                    'y': y
                },
                'Zoom': {
                    'x': z
                }
            }
        })
        self.is_moving = False

    def perform_relative_move(self, x, y, z: float):
        if self.is_moving:
            return
        self.is_moving = True
        self.ptz.RelativeMove({
            'ProfileToken': self.media_profile.token,
            'Translation': {
                'PanTilt': {
                    'x': x,
                    'y': y
                },
                'Zoom': {
                    'x': z
                }
            }
        })
        self.is_moving = False
