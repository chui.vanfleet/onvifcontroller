import yaml
from typing import List, Dict
from pydantic import BaseModel
from pathlib import Path


class Preset(BaseModel):
    preset_id: int
    name: str


class Camera(BaseModel):
    name: str
    builtin_presets: List[Preset]
    presets: List[Preset]
    ip_address: str
    port: int
    username: str
    password: str
    protocol: str
    stream_port: int
    path: str
    obs_source_name: str


class Configuration:
    web_port: int
    cameras: Dict[str, Camera] = {}

    def __init__(self, config_path: Path):
        if not config_path.exists():
            raise RuntimeError('{} does not exist'.format(config_path))
        config = yaml.full_load(config_path.read_text())
        self.web_port = int(config.get('web_port', 80))
        self.obs_websocket_url = config.get('obs_websocket_url', 'localhost:4444')
        for c in config['cameras']:
            camera = Camera(name=c['name'],
                            presets=[],
                            builtin_presets=[],
                            ip_address=c['ip_address'],
                            port=c.get('port', 80),
                            username=c['username'],
                            password=c['password'],
                            protocol=c.get('protocol', 'rtsp'),
                            stream_port=c.get('stream_port', 554),
                            path=c.get('path', 'channel1'),
                            obs_source_name=c.get('obs_source_name', ''))
            self.cameras[camera.name] = camera
            for p in c['presets']:
                camera.presets.append(Preset(preset_id=p['id'],
                                             name=p['name']))
            for p in c.get('built_in_presets', []):
                camera.builtin_presets.append(Preset(preset_id=p['id'],
                                                      name=p['name']))