from typing import Union
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QWidget, QPushButton, QGridLayout, QLabel, QSlider
from onvif_controller.gui.onvif_interface import OnvifInterface


class DPad(QWidget):
    def __init__(self):
        super().__init__()
        self.__timeout__ = 0.1
        self.__timeout_scale__ = 100
        self.__buttons__ = []
        self.__onvif__ = None
        layout = QGridLayout()
        for name, x, y in [('Up Left', -1, 1),
                           ('Up', 0, 1),
                           ('Up Right', 1, 1),
                           ('Right', 1, 0),
                           ('Down Right', 1, -1),
                           ('Down', 0, -1),
                           ('Down Left', -1, -1),
                           ('Left', -1, 0)]:
            b = self.__create_button__(name, x, y)
            self.__buttons__.append(b)
            layout.addWidget(b, 1 - y, x + 1)
        layout.addWidget(QLabel('Move'), 1, 1, Qt.AlignCenter)
        for name, zoom in [('In', 1),
                           ('Out', -1)]:
            b = self.__create_button__(name, 0, 0, zoom)
            self.__buttons__.append(b)
            layout.addWidget(b, 1 - zoom, 3)
        layout.addWidget(QLabel('Zoom'), 1, 3, Qt.AlignCenter)
        layout.addWidget(self.__create_slider__(), 3, 0, 1, 4)
        self.setLayout(layout)
        self.show()

    def __create_button__(self, name, x, y, zoom=0):
        b = QPushButton()
        b.setText(name)
        b.setDisabled(True)
        b.setProperty('vx', x)
        b.setProperty('vy', y)
        b.setProperty('vz', zoom)
        b.setProperty('name', name)
        b.clicked.connect(self.__on_click__)
        return b

    def __create_slider__(self):
        s = QSlider()
        s.setMinimum(1)
        s.setMaximum(self.__timeout_scale__)
        s.setOrientation(Qt.Horizontal)
        s.setValue(self.__timeout__ * self.__timeout_scale__)
        s.valueChanged.connect(self.__on_timeout_change__)
        return s

    def set_onvif(self, onvif: Union[OnvifInterface, None]):
        self.__onvif__ = onvif
        for b in self.__buttons__:
            b.setDisabled(onvif is None)

    # @pyqtSlot()
    def __on_timeout_change__(self, value):
        # value = 10
        self.__timeout__ = value / self.__timeout_scale__
        print('timeout changed to {}'.format(self.__timeout__))

    @pyqtSlot()
    def __on_click__(self):
        button = self.sender()
        self.__do_move__(button.property('name'), button.property('vx'), button.property('vy'), button.property('vz'))

    def __do_move__(self, name, x, y, zoom):
        if self.__onvif__:
            print('sending {} x: {} y: {} zoom: {}'.format(name, x, y, zoom))
            self.__onvif__.perform_move(x, y, zoom, self.__timeout__)
