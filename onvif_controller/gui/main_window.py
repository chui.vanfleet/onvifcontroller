import vlc
import onvif
from typing import List, Dict
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtWidgets import QWidget, QPushButton, QGridLayout, QLabel, QFrame
from onvif_controller.gui.configuration import Configuration, Camera
from onvif_controller.gui.d_pad import DPad
from onvif_controller.gui.onvif_interface import OnvifInterface


class VideoContainer:
    instance = None
    media_player = None
    frame: QFrame = None


class MainWindow(QWidget):
    connections: Dict[str, OnvifInterface] = {}
    connect_buttons: Dict[str, QPushButton] = {}
    preset_buttons: Dict[str, List[QPushButton]] = {}
    videos: Dict[str, VideoContainer] = {}
    dpads: Dict[str, DPad] = {}

    def __init__(self, config: Configuration):
        super().__init__()
        self.config = config
        self.setWindowTitle('PTZ Controller')
        layout = QGridLayout()
        header_height = 6
        cam_width = 3
        for i, name in enumerate(config.cameras.keys()):
            camera = config.cameras[name]
            camera_label = QLabel(name)
            camera_label.setAlignment(Qt.AlignCenter)
            layout.addWidget(camera_label, 0, i * cam_width, 1, cam_width)

            connect_button = QPushButton('Connect')
            connect_button.setProperty('camera_name', name)
            connect_button.clicked.connect(self.__reconnect_button__)
            self.connect_buttons[name] = connect_button
            layout.addWidget(connect_button, 1, i * cam_width, 1, cam_width)

            video = self.videos[name] = VideoContainer()
            video.instance = vlc.Instance()
            video.media_player = video.instance.media_player_new()
            video.frame = QFrame()
            palette = video.frame.palette()
            palette.setColor(QPalette.Window, QColor(0, 0, 0))
            video.frame.setPalette(palette)
            video.frame.setAutoFillBackground(True)
            layout.addWidget(video.frame, 2, i * cam_width, 1, cam_width)

            dpad = self.dpads[name] = DPad()
            layout.addWidget(dpad, header_height, i * cam_width + 2, 4, 1)

            layout.setRowStretch(2, 3)
            layout.setRowMinimumHeight(2, 200)

            for j, preset in enumerate(camera.presets):
                layout.addWidget(QLabel(preset.name), j + header_height, i * cam_width)
                button = QPushButton("Call")
                button.setProperty('camera_name', name)
                button.setProperty('preset_name', preset.name)
                button.clicked.connect(self.__call_preset__)
                button.setDisabled(True)
                self.preset_buttons.setdefault(name, []).append(button)
                layout.addWidget(button, j + header_height, i * cam_width + 1)

        self.setLayout(layout)
        self.show()
        for camera in self.config.cameras.values():
            self.__connect_to_camera__(camera)

    @pyqtSlot()
    def __call_preset__(self):
        state = self.sender()
        camera = self.config.cameras[state.property('camera_name')]
        preset = [p for p in camera.presets if p.name == state.property('preset_name')][0]
        if camera.name in self.connections:
            print('calling preset for {}: {}'.format(camera.name, preset.name))
            self.connections[camera.name].call_preset(preset.preset_id)

    @pyqtSlot()
    def __reconnect_button__(self):
        state = self.sender()
        camera = self.config.cameras[state.property('camera_name')]
        self.__connect_to_camera__(camera)

    def __connect_to_camera__(self, camera: Camera):
        try:
            video = self.videos[camera.name]
            video.media_player.video_set_mouse_input(False)
            video.media_player.video_set_key_input(False)
            video.media_player.set_mrl('{}://{}:{}/{}'.format(camera.protocol, camera.ip_address, camera.stream_port, camera.path))
            video.media_player.audio_set_mute(True)
            video.media_player.set_xwindow(video.frame.winId())
            video.media_player.play()

            self.connections[camera.name] = OnvifInterface(camera.ip_address, camera.port, camera.username, camera.password)
            self.dpads[camera.name].set_onvif(self.connections[camera.name])
            self.connect_buttons[camera.name].setText('Reconnect')
            for b in self.preset_buttons.get(camera.name):
                b.setDisabled(False)
        except onvif.ONVIFError as e:
            self.connect_buttons[camera.name].setText('Connect')
            self.dpads[camera.name].set_onvif(None)
            print('failed to connect to "{}" at {}'.format(camera.name, camera.ip_address), e)