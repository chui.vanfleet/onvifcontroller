import argparse
import sys
from math import sqrt

import uvicorn
from pydantic import BaseModel
from typing import List, Dict
from pathlib import Path
from starlette.applications import Starlette
from starlette.responses import FileResponse
from fastapi import FastAPI, HTTPException
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from onvif_controller.gui.configuration import Configuration, Camera
from onvif_controller.gui.onvif_interface import OnvifInterface

app = Starlette()
api = FastAPI(title='Onvif Controller API')
frontend = Starlette()

pkg_dir = str(Path(__file__).absolute().parent)
sys.path.insert(0, pkg_dir)


@frontend.middleware("http")
async def add_custom_header(request, call_next):
    response = await call_next(request)
    if response.status_code == 404:
        return FileResponse(pkg_dir + '/static/index.html')
    return response


@frontend.exception_handler(404)
def not_found(request, exc):
    return FileResponse(pkg_dir + '/static/index.html')


frontend.mount('/static', StaticFiles(directory=pkg_dir + '/static/static'), name='static')
frontend.mount('/', StaticFiles(directory=pkg_dir + '/static'), name='static')  # ??
app.mount('/api', app=api)
app.mount('/', app=frontend)

origins = [
    'http://localhost:3000',
    'http://localhost:8080',
]
app.add_middleware(CORSMiddleware,
                   allow_origins=origins,
                   allow_credentials=True,
                   allow_methods=['*'],
                   allow_headers=['*'])

parser = argparse.ArgumentParser()
parser.add_argument('config', type=Path, help='path to configuration file')
args = parser.parse_args()
config = Configuration(args.config)

cameras: Dict[str, OnvifInterface] = {n: OnvifInterface(c.ip_address, c.port, c.username, c.password) for n, c in config.cameras.items()}


class PostPreset(BaseModel):
    camera_name: str
    id: int


class PostMove(BaseModel):
    camera_name: str
    x: float
    y: float
    z: float


class ObsResponse(BaseModel):
    obs_websocket_url: str


@api.get('/obs', response_model=ObsResponse)
def get_obs():
    return ObsResponse(obs_websocket_url=config.obs_websocket_url)


@api.get('/cameras', response_model=List[Camera])
def get_cameras():
    for name, camera in cameras.items():
        if not camera.camera:
            camera.reconnect()
    return list(config.cameras.values())


@api.post('/preset')
def call_preset(args: PostPreset):
    camera_name = args.camera_name
    id = args.id
    if not camera_name in cameras:
        raise HTTPException(detail='No camera by name of {}'.format(camera_name), status_code=404)
    try:
        cameras[camera_name].call_preset(id)
    except Exception as e:
        raise HTTPException(detail='Failed to call preset: {}'.format(e), status_code=500)


@api.post('/set_preset')
def set_preset(args: PostPreset):
    camera_name = args.camera_name
    id = args.id
    if not camera_name in cameras:
        raise HTTPException(detail='No camera by name of {}'.format(camera_name), status_code=404)
    try:
        cameras[camera_name].set_preset(id)
    except Exception as e:
        raise HTTPException(detail='Failed to call preset: {}'.format(e), status_code=500)


@api.post('/absolute_move')
def call_absolute_move(args: PostMove):
    camera_name = args.camera_name
    if not camera_name in cameras:
        raise HTTPException(detail='No camera by name of {}'.format(camera_name), status_code=404)
    try:
        cameras[camera_name].perform_absolute_move(args.x, args.y, args.z)
    except Exception as e:
        raise HTTPException(detail='Failed to call preset: {}'.format(e), status_code=500)


@api.post('/relative_move')
def call_relative_move(args: PostMove):
    camera_name = args.camera_name
    if not camera_name in cameras:
        raise HTTPException(detail='No camera by name of {}'.format(camera_name), status_code=404)
    try:
        cameras[camera_name].perform_relative_move(args.x, args.y, args.z)
    except Exception as e:
        raise HTTPException(detail='Failed to call preset: {}'.format(e), status_code=500)


@api.post('/continuous_move')
def call_continuous_move(args: PostMove):
    camera_name = args.camera_name
    if not camera_name in cameras:
        raise HTTPException(detail='No camera by name of {}'.format(camera_name), status_code=404)
    try:
        cameras[camera_name].perform_move(args.x, args.y, args.z, timeout=sqrt(args.x ** 2 + args.y ** 2 + args.z ** 2))
    except Exception as e:
        raise HTTPException(detail='Failed to call preset: {}'.format(e), status_code=500)


def main():
    uvicorn.run('onvif_controller.web_controller:app', host='0.0.0.0', port=config.web_port, workers=1)


if __name__ == '__main__':
    main()
