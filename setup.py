#!/usr/bin/env python

import sys
import shutil
from setuptools import setup
from pathlib import Path

pkg_name = 'onvif_controller'

setup(name=pkg_name,
      version='1.0',
      description='Preset Controller for Onvif PTZ Cameras',
      author='Chui Vanfleet',
      author_email='chui.vanfleet@gmail.com',
      packages=[
          pkg_name,
          pkg_name + '/gui',
      ],
      entry_points={
          'console_scripts': [
              'onvif_web_controller = onvif_controller.web_controller:main',
              'onvif_preset_selector = onvif_controller.preset_selector:main'
          ]
      },
      install_requires=[
          'onvif-zeep',
          'pyqt5',
          'argparse',
          'python-vlc',
          'xbox360controller',
          'pydantic',
          'pyyaml',
          'fastapi',
          'uvicorn',
          'aiofiles',
      ],
     )

DATA_DIR = Path(sys.prefix, 'lib', 'python{}.{}'.format(sys.version_info.major, sys.version_info.minor), 'site-packages', pkg_name, 'static')
static_folder = Path(__file__).absolute().parent / 'web_ui' / 'build'

shutil.rmtree(DATA_DIR, ignore_errors=True)
shutil.copytree(static_folder, DATA_DIR)