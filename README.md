# Onvif Controller

This is just a basic application to connect to multiple onvif capable cameras and show:

* rtsp stream
* call presets
* PTZ control
    * up/down/left/right
    * "speed"
    * zoom

## Installation

### Getting the Code
```bash
git clone https://gitlab.com/chui.vanfleet/onvifcontroller.git
cd onvifcontroller
```

### Build the Web UI

```bash
cd web_ui
npm install
npm run-script build
```

### Build the Web Backend

You'll want to make sure that `onvifcontroller/static` is a symbolic link to `web_ui/build`. ie, `ll` should show `static -> ../web_ui/build/`

```bash
python3 -m venv venv
source venv/bin/activate
pip3 install .
```

It may be useful to prove that everything is working by running the web controller manually first:

```bash
(venv) onvifcontroller$ python3 onvif_controller/web_controller.py /path/to/camera_configuration.yml
```

### Setting up a User Service

This section will help you install a user service that will start when you log-in.

1. First you'll need to create a service file in `~/.config/systemd/user.control/onvif_web_controller.service`
2. Add the following to the service file, adjusting parameters or paths as needed:
```
[Unit]
Description=Onvif Web Controller
Wants=network-online.target
After=network-online.target

[Service]
WorkingDirectory=/path/to/onvifcontroller
ExecStart=/bin/bash -c "source venv/bin/activate; python3 onvif_controller/web_controller.py /path/to/camera_configuration.yml"
KillMode=control-group
KillSignal=SIGINT

[Install]
WantedBy=default.target
```
3. run `systemctl --user daemon-reload` after making any changes to the service file
4. `systemctl --user enable onvif_web_controller.service` will cause this service to start when you log in.
5. `systemctl --user start onvif_web_controller.service` will start the service.

# Desktop Version
to use, just run (in the venv created above)

```bash
onvif_preset_selector onvif_controller_config.yml
```

![](documentation/PTZ%20Controller%20Window.png)

The web version can be run with

```bash
onvif_web_controller onvif_controller_config.yml
```

![](documentation/Web%20View.png)

## Configuration

The configuration file is yaml format. For example:

```yaml
web_port: 8080  # port to use to access web application
obs_websocket_url: vulture.local:4444  # ip address and port for the obs websocket plugin.
cameras:
  - name: Right Camera
    ip_address: 192.168.1.137
    port: 80  # optional: onvif connection port. defaults to 80
    username: user  # username for onvif
    password: user  # username for onvif
    protocol: rtsp  # optional: defaults to 'rtsp'
    stream_port: 554  # optional: port to use for rtsp stream. defaults to 554
    path: channel1  # optional: path appended to url. ie, with these configs: 'rtsp://192.168.1.137:554/channel1'
    obs_source_name: Colors/Red Color  # optional: in order to show which camera is in the obs program. <scene_name>/<source_name>
    builtin_presets:
      - id: 132  # preset number
        name: IR Mode Off  # Preset name, only used in the gui.
    presets:
      - id: 1  # preset number
        name: Tall Speaker  # Preset name, only used in the gui.
  - name: Left Camera
    ip_address: localhost
    username: user
    password: user
    presets:
      - id: 1
        name: Tall Speaker
      - id: 2
        name: Short Speaker
      - id: 135
        name: IR Mode Off
      - id: 12
        name: Tall Speaker
      - id: 22
        name: Short Speaker
```