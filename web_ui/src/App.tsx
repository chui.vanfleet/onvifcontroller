import React from 'react';
import './App.css';
import {Switch, Route} from "react-router-dom";
import {AppBar, Toolbar} from "@material-ui/core";
import Overview from "./components/Overview";

function App() {
    return (
        <div className="App">
            <AppBar position="static">
                <Toolbar>
                    <h1>PTZ Controller</h1>
                </Toolbar>
            </AppBar>
            <Switch>
                <Route component={Overview} />
            </Switch>
        </div>
    );
}

export default App;
