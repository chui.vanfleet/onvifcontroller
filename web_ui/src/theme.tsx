import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
    palette: {
        type: 'light',
        primary: {
            light: '#56c7d6',
            main: '#0096a5',
            dark: '#006876',
            contrastText: '#000000',
        },
        secondary: {
            light: '#ffd95a',
            main: '#f9a825',
            dark: '#c17900',
            contrastText: '#000000',
        },
    },
});

export default theme;