
class Api {
    private base_path = process.env.REACT_APP_API_BASE;

    get_obs() {
        return fetch(this.base_path + '/obs');
    }

    get_cameras() {
        return fetch(this.base_path + '/cameras');
    }

    post_move(path: string, camera_name: string, x: number, y: number, z: number) {
        return fetch(this.base_path + path,
            {
                method: 'POST',
                body: JSON.stringify({camera_name: camera_name, x: x, y: y, z: z})
            });

    }

    post_preset(camera_name: string, id: number) {
        return fetch(this.base_path + '/preset',
            {
                method: 'POST',
                body: JSON.stringify({camera_name: camera_name, id: id})
            });
    }

    set_preset(camera_name: string, id: number) {
        return fetch(this.base_path + '/set_preset',
            {
                method: 'POST',
                body: JSON.stringify({camera_name: camera_name, id: id})
            });
    }

    post_absolute_move(camera_name: string, x: number, y: number, z: number) {
        return this.post_move('/absolute_move', camera_name, x, y, z);
    }

    post_relative_move(camera_name: string, x: number, y: number, z: number) {
        return this.post_move('/relative_move', camera_name, x, y, z);
    }

    post_continuous_move(camera_name: string, vx: number, vy: number, z: number) {
        return this.post_move('/continuous_move', camera_name, vx, vy, z);
    }
}

const api = new Api();

export default api;