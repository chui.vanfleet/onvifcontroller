import React from "react";
import {createStyles, Theme, withStyles} from "@material-ui/core";

const styles = (theme: Theme) => createStyles({
    root: {
    },
});

interface TemplateProps {
    classes?: any;
}

interface TemplateState {
}

class Template extends React.Component<TemplateProps, TemplateState> {
    constructor(props: TemplateProps) {
        super(props);
        this.state = {
        };
    }

    render() {
        return <div/>
    }
}

export default withStyles(styles)(Template)