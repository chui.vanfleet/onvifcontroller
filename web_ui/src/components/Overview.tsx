import React from "react";
import {Theme} from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import {withStyles, createStyles} from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import api from "../api";
import CameraCard from "./CameraCard";
import ObsWebsocket, {Scene, SceneItem, SceneItemTransform} from "obs-websocket-js";

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const styles = (theme: Theme) => createStyles({
    root: {
    },
    camera_list: {
        display: 'flex',
    }
});

export interface Preset {
    preset_id: number;
    name: string;
}

export interface Camera {
    name: string;
    builtin_presets: Preset[];
    presets: Preset[];
    ip_address: string;
    port: number;
    username: string;
    password: string;
    protocol: string;
    stream_port: number;
    path: string;
    obs_source_name: string;
}

interface ObsConfig {
    obs_websocket_url: string;
}

interface OverviewProps {
    classes?: any;
}

interface OverviewState {
    cameras: Camera[];
    camera_visibility_map: {[camera_name: string]: boolean};
    error_msg: string;
    obs: ObsWebsocket;
    obs_config: ObsConfig;
    current_scene: Scene | undefined;
}

class Overview extends React.Component<OverviewProps, OverviewState> {
    constructor(props: OverviewProps) {
        super(props);
        this.state = {
            cameras: [],
            camera_visibility_map: {},
            error_msg: '',
            obs: new ObsWebsocket(),
            obs_config: {obs_websocket_url: ''},
            current_scene: undefined,
        }
    }

    async componentDidMount() {
        await this.getCameras();
        try {
            const response = await api.get_obs();
            if (response.ok) {
                const obs: ObsConfig = await response.json();
                await this.state.obs.connect({address: obs.obs_websocket_url});
                console.info('connected to obs.');

                this.state.obs.on('SwitchScenes', (data: any) => {
                    // since the source state can change after the transition has been made, try to capture the
                    // mute state of each camera source to track which cameras are not in the program.
                    this.state.cameras.forEach((camera) => {
                        console.info('checking visibility state of ', camera.obs_source_name);
                        const split_index = camera.obs_source_name.indexOf('/');
                        const scene_name = camera.obs_source_name.slice(0, split_index);
                        const item_name = camera.obs_source_name.slice(split_index + 1);
                        this.state.obs.send('GetSceneItemProperties', {'scene-name': scene_name, 'item': {'name': item_name}}).then((data: SceneItemTransform) => {
                            let visibility_map = this.state.camera_visibility_map;
                            visibility_map[camera.obs_source_name] = data.visible;
                            this.setState({camera_visibility_map: visibility_map});
                        }).catch(e => {
                            this.setState({error_msg: 'Failed to get scene-item properties: ' + e.toString()});
                        });
                    });
                });
            }
        } catch (e) {
            this.setState({error_msg: 'Failed to connect to obs: ' + JSON.stringify(e)});
        }
    }

    async getCameras() {
        const response = await api.get_cameras();
        if (response.ok) {
            const cameras: Camera[] = await response.json();
            this.setState({cameras: cameras});
        } else {
            this.setState({error_msg: await response.text()});
        }
    }

    render() {
        const {classes} = this.props;
        const {cameras, error_msg, camera_visibility_map} = this.state;
        return <div>
            <div className={classes.camera_list}>
                {cameras.map((camera: Camera, i: number) =>
                    <CameraCard key={camera.name + camera.ip_address + i.toString()} camera={camera} on_program={camera_visibility_map[camera.obs_source_name]}/>
                )}
            </div>
            <Snackbar open={error_msg.length > 0} autoHideDuration={6000} onClose={()=> this.setState({error_msg: ''})}>
                <Alert severity="error">{error_msg}</Alert>
            </Snackbar>
        </div>
    }
}

export default withStyles(styles)(Overview)