import React from "react";
import {Button, createStyles, Menu, MenuItem, Snackbar, Theme, withStyles} from "@material-ui/core";
import {Preset} from "./Overview";
import api from "../api";
import {Alert} from "@material-ui/lab";

const styles = (theme: Theme) => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        padding: 8,
        margin: 8,
    },
    button: {
        width: '100%',
        margin: 8,
    }
});

interface MousePoint {
    x: number;
    y: number
}

interface CameraPresetsListProps {
    classes?: any;
    camera_name: string;
    presets: Preset[];
}

interface CameraPresetsListState {
    mouse_point: null | MousePoint;
    selected_preset: null | Preset;
    error_msg: string;
}

class CameraPresetsList extends React.Component<CameraPresetsListProps, CameraPresetsListState> {
    constructor(props: CameraPresetsListProps) {
        super(props);
        this.state = {
            mouse_point: null,
            error_msg: '',
            selected_preset: null
        };
        this.onClosePresetMenu = this.onClosePresetMenu.bind(this);
        this.onSetPresetFromCurrentPosition = this.onSetPresetFromCurrentPosition.bind(this);
    }

    async onClickPreset(preset: Preset) {
        const response = await api.post_preset(this.props.camera_name, preset.preset_id);
        if (!response.ok) {
            const error = await response.json();
            this.setState({error_msg: 'Failed to send ' + preset.name + ' preset: ' + error.detail});
        }
    }

    async onRightClickPreset(event: React.MouseEvent<HTMLButtonElement, MouseEvent>, preset: Preset) {
        event.preventDefault();
        this.setState({mouse_point: {
            x: event.clientX - 2,
            y: event.clientY - 4,
        },
        selected_preset: preset});
    }

    async onClosePresetMenu() {
        this.setState({mouse_point: null, selected_preset: null});
    }

    async onSetPresetFromCurrentPosition() {
        const {selected_preset} = this.state;
        if (selected_preset) {
            const response = await api.set_preset(this.props.camera_name, selected_preset?.preset_id);
            if (!response.ok) {
                const error = await response.json();
                this.setState({error_msg: 'Failed to set ' + selected_preset.name + ' preset: ' + error.detail});
            }
        }
        await this.onClosePresetMenu();
    }

    render() {
        const {presets, classes} = this.props;
        const {error_msg, mouse_point} = this.state;
        return <div className={classes.root}>
            {presets.map((preset: Preset, i: number) =>
                <Button key={preset.name + preset.preset_id.toString() + i.toString()}
                        className={classes.button}
                        variant="contained"
                        onContextMenu={(e) => this.onRightClickPreset(e, preset)}
                        onClick={() => this.onClickPreset(preset)}>
                    {preset.name}
                </Button>
            )}
            <Menu open={mouse_point !== null}
                  keepMounted
                  onClose={this.onClosePresetMenu}
                  anchorReference="anchorPosition"
                  anchorPosition={
                      mouse_point !== null
                          ? {top: mouse_point.y, left: mouse_point.x}
                          : undefined
                  }>
                <MenuItem onClick={this.onSetPresetFromCurrentPosition}>Set from current position</MenuItem>
            </Menu>
            <Snackbar open={error_msg.length > 0} autoHideDuration={6000} onClose={()=> this.setState({error_msg: ''})}>
                <Alert severity="error">{error_msg}</Alert>
            </Snackbar>
        </div>
    }
}

export default withStyles(styles)(CameraPresetsList)