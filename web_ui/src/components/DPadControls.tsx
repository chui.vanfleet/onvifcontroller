import React from "react";
import {Button, createStyles, Grid, IconButton, Snackbar, Theme, Tooltip, withStyles} from "@material-ui/core";
import Slider from '@material-ui/core/Slider';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import HomeIcon from '@material-ui/icons/Home';
import Icon from "@mdi/react";
import {mdiMagnifyMinusOutline, mdiMagnifyPlusOutline, mdiRabbit, mdiTortoise} from '@mdi/js';
import api from "../api";
import {Alert} from "@material-ui/lab";

const styles = (theme: Theme) => createStyles({
    root: {
        padding: 8,
        margin: 8,
    },
    dpad: {
        display: 'flex',
        width: 170,
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    dpad_button: {
    },
    zoom_buttons: {
        padding: 0,
    },
    button: {
        width: '100%',
        margin: 8,
    }
});

export type ControlType = 'continuous' | 'absolute' | 'relative';

interface DPadControlsProps {
    classes?: any;
    camera_name: string;
    control_type: ControlType;
}

interface DPadControlsState {
    move_magnitude: number;
    zoom_magnitude: number;
    error_msg: string;
}

class DPadControls extends React.Component<DPadControlsProps, DPadControlsState> {
    constructor(props: DPadControlsProps) {
        super(props);
        this.state = {
            move_magnitude: 0.1,
            zoom_magnitude: 1.0,
            error_msg: '',
        };
        this.onMoveSliderChange = this.onMoveSliderChange.bind(this);
        this.onZoomSliderChange = this.onZoomSliderChange.bind(this);
    }

    async onDirectionClick(x: number, y: number) {
        const name = this.props.camera_name;
        x = x * this.state.move_magnitude;
        y = y * this.state.move_magnitude;
        let response: Response;
        switch (this.props.control_type) {
            case "absolute":
                response = await api.post_absolute_move(name, x, y, 0.0);
                break;
            case "continuous":
                response = await api.post_continuous_move(name, x, y, 0.0);
                break;
            case "relative":
                response = await api.post_relative_move(name, x, y, 0.0);
                break;
        }
        if (!response.ok) {
            const error = await response.json();
            this.setState({error_msg: 'Failed to send command: ' + error.detail});
        }
    }

    async onHomeClick() {

    }

    async onZoomClick(d: number) {
        const name = this.props.camera_name;
        d = d * this.state.zoom_magnitude;
        let response: Response;
        switch (this.props.control_type) {
            case "absolute":
                response = await api.post_absolute_move(name, 0.0, 0.0, d);
                break;
            case "continuous":
                response = await api.post_continuous_move(name, 0.0, 0.0, d);
                break;
            case "relative":
                response = await api.post_relative_move(name, 0.0, 0.0, d);
                break;
        }
        if (!response.ok) {
            const error = await response.json();
            this.setState({error_msg: 'Failed to send command: ' + error.detail});
        }
    }

    async onRefocus() {
        const name = this.props.camera_name;
        const response = await api.post_continuous_move(name, 0.0, 0.0, 0.0001);
        if (!response.ok) {
            const error = await response.json();
            this.setState({error_msg: 'Failed to send command: ' + error.detail});
        }
    }

    onMoveSliderChange(event: any, newValue: number | number[]) {
        this.setState({move_magnitude: newValue as number});
    }

    onZoomSliderChange(event: any, newValue: number | number[]) {
        this.setState({zoom_magnitude: newValue as number});
    }

    render() {
        const directions = [
            {x: -1, y: 1},
            {x: 0, y: 1},
            {x: 1, y: 1},

            {x: -1, y: 0},
            {x: 0, y: 0},
            {x: 1, y: 0},

            {x: -1, y: -1},
            {x: 0, y: -1},
            {x: 1, y: -1},
        ];

        const {classes, camera_name} = this.props;
        const {error_msg} = this.state;
        return <div className={classes.root}>
            <div className={classes.dpad}>
                {directions.map((d, i: number) => {
                    if (d.x === 0 && d.y === 0) {
                        return <IconButton key={camera_name + i.toString()}
                                           className={classes.dpad_button}
                                           onClick={() => this.onHomeClick()}>
                            <HomeIcon/>
                        </IconButton>
                    } else {
                        return <IconButton key={camera_name + i.toString()}
                                           className={classes.dpad_button}
                                           onClick={() => this.onDirectionClick(d.x, d.y)}>
                            <ArrowRightIcon style={{transform: 'rotate(' + (Math.atan2(d.x, d.y) - Math.PI / 2).toString() + 'rad)'}}/>
                        </IconButton>
                    }
                })}
            </div>
            <Tooltip title="Slide to adjust how quickly the camera moves around." enterDelay={6000}>
                <Grid container spacing={2}>
                    <Grid item>
                        <Icon className={classes.zoom_buttons} path={mdiTortoise} size={1}/>
                    </Grid>
                    <Grid item xs>
                        <Slider value={this.state.move_magnitude} max={1.0} min={0.0001} step={0.0001} onChange={this.onMoveSliderChange}/>
                    </Grid>
                    <Grid item>
                        <Icon className={classes.zoom_buttons} path={mdiRabbit} size={1}/>
                    </Grid>
                </Grid>
            </Tooltip>
            <Tooltip title="Slide to adjust how quickly to zoom in or out." enterDelay={6000}>
                <Grid container spacing={2}>
                    <Grid item>
                        <IconButton className={classes.zoom_buttons}
                                    onClick={() => this.onZoomClick(-1)}>
                            <Icon path={mdiMagnifyMinusOutline} size={1}/>
                        </IconButton>
                    </Grid>
                    <Grid item xs>
                        <Slider value={this.state.zoom_magnitude}
                                max={3.0}
                                min={0.0001}
                                step={0.001}
                                onChange={this.onZoomSliderChange}/>
                    </Grid>
                    <Grid item>
                        <IconButton className={classes.zoom_buttons}
                                    onClick={() => this.onZoomClick(1)}>
                            <Icon path={mdiMagnifyPlusOutline} size={1}/>
                        </IconButton>
                    </Grid>
                </Grid>
            </Tooltip>
            <Button className={classes.button}
                    variant="contained"
                    onClick={() => this.onRefocus()}>
                Refocus
            </Button>
            <Snackbar open={error_msg.length > 0} autoHideDuration={6000} onClose={()=> this.setState({error_msg: ''})}>
                <Alert severity="error">{error_msg}</Alert>
            </Snackbar>
        </div>
    }
}

export default withStyles(styles)(DPadControls)