import React from "react";
import clsx from 'clsx';
import {
    Avatar,
    Card,
    CardContent,
    CardHeader,
    createStyles, Divider,
    IconButton,
    Menu, MenuItem,
    Theme,
    withStyles
} from "@material-ui/core";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { red } from '@material-ui/core/colors';
import {Camera, Preset} from "./Overview";
import CameraPresetsList from "./CameraPresetsList";
import DPadControls, {ControlType} from "./DPadControls";
import api from "../api";

const styles = (theme: Theme) => createStyles({
    root: {
        margin: 8,
    },
    avatar: {
      backgroundColor: red[500],
    },
    camera_in_use: {
        backgroundColor: theme.palette.error.light + '50',
    },
    card_content: {
        display: 'flex',
    }
});

let control_type_to_pretty = new Map<ControlType, string>();
control_type_to_pretty.set('continuous', 'Continuous');
control_type_to_pretty.set('absolute', 'Absolute');
control_type_to_pretty.set('relative', 'Relative');

interface CameraCardProps {
    classes?: any;
    camera: Camera;
    on_program: boolean;
}

interface CameraCardState {
    control_type: ControlType;
    menu_anchor: null | HTMLElement;
}

class CameraCard extends React.Component<CameraCardProps, CameraCardState> {
    constructor(props: CameraCardProps) {
        super(props);
        this.state = {
            control_type: 'continuous',
            menu_anchor: null,
        };
        this.handleMenuOpen = this.handleMenuOpen.bind(this);
        this.handleCloseMenu = this.handleCloseMenu.bind(this);
    }

    handleMenuOpen(event: any) {
        this.setState({menu_anchor: event.currentTarget});
      }

    async handleCloseMenu() {
        this.setState({menu_anchor: null});
      }

    handleSelectControlType(type: ControlType) {
        this.handleCloseMenu();
        this.setState({control_type: type});
    }

    async handleCallBuiltinPreset(preset: Preset) {
        await this.handleCloseMenu();
        const response = await api.post_preset(this.props.camera.name, preset.preset_id);
        if (!response.ok) {
            // const error = await response.json();
            // this.setState({error_msg: 'Failed to send ' + preset.name + ' preset: ' + error.detail});
        }
    }

    render() {
        const {classes, camera, on_program} = this.props;
        const avatar = <Avatar className={classes.avatar}>
                {camera.name[0]}
            </Avatar>
        const subheader = <div>
            <span>{camera.ip_address}</span>
            <span>-</span>
            <span>{control_type_to_pretty.get(this.state.control_type)}</span>
        </div>
        const card_action = <IconButton aria-label="settings">
            <MoreVertIcon onClick={this.handleMenuOpen}/>
          </IconButton>
        return <Card className={clsx(classes.root, {
            [classes.camera_in_use]: on_program,
        })}>
            <CardHeader avatar={avatar}
                        title={camera.name}
                        subheader={subheader}
                        action={card_action}/>
            <CardContent className={classes.card_content}>
                <DPadControls camera_name={camera.name} control_type={this.state.control_type}/>
                <CameraPresetsList camera_name={camera.name} presets={camera.presets}/>
            </CardContent>

            <Menu open={Boolean(this.state.menu_anchor)}
                  anchorEl={this.state.menu_anchor}
                  onClose={this.handleCloseMenu}
                  keepMounted>
                <MenuItem onClick={()=>this.handleSelectControlType('continuous')}>Continuous Move</MenuItem>
                <MenuItem onClick={()=>this.handleSelectControlType('absolute')}>Absolute Move</MenuItem>
                <MenuItem onClick={()=>this.handleSelectControlType('relative')}>Relative Move</MenuItem>
                <Divider/>
                {camera.builtin_presets.map((preset: Preset, i: number) =>
                    <MenuItem key={preset.name + i.toString()} onClick={()=>this.handleCallBuiltinPreset(preset)}>{preset.name}</MenuItem>
                )}
            </Menu>
        </Card>;
    }
}

export default withStyles(styles)(CameraCard)